package pl.r4fall.gymtrainer;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import pl.r4fall.gymtrainer.logic.TrainingPlanCreator;
import pl.r4fall.gymtrainer.logic.UserProfileIO;
import pl.r4fall.gymtrainer.model.Exercise;
import pl.r4fall.gymtrainer.model.Training;
import pl.r4fall.gymtrainer.model.TrainingPlan;
import pl.r4fall.gymtrainer.model.User;
import pl.r4fall.gymtrainer.model.spec.DayOfWeek;
import pl.r4fall.gymtrainer.model.spec.ExerciseType;
import pl.r4fall.gymtrainer.model.spec.MuscleGroup;
import pl.r4fall.gymtrainer.model.spec.TrainingTarget;

import static org.junit.Assert.*;

public class LogicTest {
    private final static Logger log = LoggerFactory.getLogger(LogicTest.class);

    @Test
    public void testSerialization() throws IOException, ClassNotFoundException {
        assertFalse(UserProfileIO.isUserProfileExist(getFileDir()));

        User user = new User();
        user.setHashedPassword("pass");
        user.setAge(25);
        user.setHeight(192);
        user.setWeight(87);

        TrainingPlan trainingPlan = new TrainingPlan();
        Map<Calendar, Training> trainingMap = new HashMap<>();
        Training training = new Training();
        List<Exercise> exercises = new ArrayList<>();
        Exercise exercise = new Exercise(ExerciseType.CARDIO, 15, 1);
        exercises.add(exercise);
        training.setExercises(exercises);
        trainingMap.put(Calendar.getInstance(), training);

        trainingPlan.setAllTrainings(trainingMap);
        user.setTrainingPlan(trainingPlan);

        UserProfileIO.createUserProfile(getFileDir(), user);
        assertTrue(UserProfileIO.isUserProfileExist(getFileDir()));

        User readuser = UserProfileIO.getUserProfile(getFileDir());
        assertNotNull(readuser);
        assertEquals(user.getHashedPassword(), readuser.getHashedPassword());

        UserProfileIO.removeUserProfile(getFileDir());
        assertFalse(UserProfileIO.isUserProfileExist(getFileDir()));
    }

    @Test
    public void testPlanCreation() {
        int daysCount = 10;
        Map<Calendar, Training> previous = new HashMap<>();

        Set<DayOfWeek> dayOfWeekSet = new HashSet<>();
        dayOfWeekSet.addAll(Arrays.asList(DayOfWeek.MONDAY, DayOfWeek.TUESDAY, DayOfWeek.SATURDAY, DayOfWeek.SUNDAY));

        Map<Calendar, Training> trainings = TrainingPlanCreator.getTrainings(previous, dayOfWeekSet, daysCount, TrainingTarget.CONDITION_IMPROVE, 101, 1.82f, 25);

        List<Calendar> days = new ArrayList<>(trainings.keySet());
        Collections.sort(days);
        for (Calendar calendar : days) {
            log.info("------------------");
            String theDate =  calendar.get(Calendar.DAY_OF_MONTH) + "." + calendar.get(Calendar.MONTH) + "." + calendar.get(Calendar.YEAR) + " (" + calendar.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH) + ")";
            log.info(theDate);

            List<Exercise> exercises = trainings.get(calendar).getExercises();
            Set<MuscleGroup> muscleGroups = new HashSet<>();
            for (Exercise exercise : exercises) {
                muscleGroups.add(exercise.getExerciseType().getMuscleGroup());
            }
            List<MuscleGroup> muscles = new ArrayList<>(muscleGroups);
            Collections.sort(muscles);

            log.info(muscles.toString());
            log.info("-------------------\n");
        }

        assertEquals(daysCount, trainings.size());
    }

    private File getFileDir() {
        return new File(System.getProperty("user.home"), "gym_trainer");
    }
}