package pl.r4fall.gymtrainer.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import pl.r4fall.gymtrainer.R;
import pl.r4fall.gymtrainer.element.ActivityAlert;
import pl.r4fall.gymtrainer.model.spec.DayOfWeek;
import pl.r4fall.gymtrainer.model.spec.TrainingTarget;
import pl.r4fall.gymtrainer.util.Message;

public class SettingsActivity extends Activity {
    public final static String TRAINING_TARGET = "training_target";
    public final static String AGE = "age";
    public final static String HEIGHT = "height";
    public final static String WEIGHT = "weight";
    public final static String DAYS = "days";
    public final static String CORRECT = "correct";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Intent intent = getIntent();
        final boolean isRestore = intent.getBooleanExtra(HomeActivity.RESTORE_SETTINGS, false);

        if (!isRestore)
            ActivityAlert.showAlert(SettingsActivity.this, Message.INSTANCE.get("settings.noPlan.title"), Message.INSTANCE.get("settings.noPlan.message"));

        final RadioGroup goalRadioGroup = (RadioGroup) findViewById(R.id.a_setting_goal);
        final EditText age = (EditText) findViewById(R.id.a_setting_age);
        final EditText height = (EditText) findViewById(R.id.a_setting_height);
        final EditText weight = (EditText) findViewById(R.id.a_setting_weight);

        final Map<DayOfWeek, CheckBox> days = new HashMap<>(7);
        days.put(DayOfWeek.MONDAY, (CheckBox) findViewById(R.id.a_setting_monday));
        days.put(DayOfWeek.TUESDAY, (CheckBox) findViewById(R.id.a_setting_tuesday));
        days.put(DayOfWeek.WEDNESDAY, (CheckBox) findViewById(R.id.a_setting_wednesday));
        days.put(DayOfWeek.THURSDAY, (CheckBox) findViewById(R.id.a_setting_thursday));
        days.put(DayOfWeek.FRIDAY, (CheckBox) findViewById(R.id.a_setting_friday));
        days.put(DayOfWeek.SATURDAY, (CheckBox) findViewById(R.id.a_setting_saturday));
        days.put(DayOfWeek.SUNDAY, (CheckBox) findViewById(R.id.a_setting_sunday));

        if (isRestore) {
            TrainingTarget trainingTarget = (TrainingTarget) intent.getSerializableExtra(SettingsActivity.TRAINING_TARGET);
            int intentAge = intent.getIntExtra(SettingsActivity.AGE, 25);
            float intentHeight = intent.getFloatExtra(SettingsActivity.HEIGHT, 1.8f);
            float intentWeight = intent.getFloatExtra(SettingsActivity.WEIGHT, 90.0f);
            ArrayList intentDays = (ArrayList) intent.getSerializableExtra(SettingsActivity.DAYS);

            if (trainingTarget == TrainingTarget.FORCE_INCREASE)
                ((RadioButton) findViewById(R.id.a_setting_muscle_up)).setChecked(true);
            else if (trainingTarget == TrainingTarget.WEIGHT_REDUCTION)
                ((RadioButton) findViewById(R.id.a_setting_weight_loss)).setChecked(true);
            else if (trainingTarget == TrainingTarget.CONDITION_IMPROVE)
                ((RadioButton) findViewById(R.id.a_setting_fit_and_health)).setChecked(true);

            age.setText(String.valueOf(intentAge));
            height.setText(String.valueOf((int) (intentHeight * 100)));
            weight.setText(String.valueOf(intentWeight));

            for (DayOfWeek dayOfWeek : days.keySet())
                if (intentDays.contains(dayOfWeek))
                    days.get(dayOfWeek).setChecked(true);
        }

        ImageButton okButton = (ImageButton) findViewById(R.id.a_setting_ok);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!isGoalValid(goalRadioGroup) || !isAgeValid(age) || !isHeightValid(height) ||
                        !isWeightValid(weight) || !isAvailableDaysValid(days.values())) {
                    ActivityAlert.showAlert(SettingsActivity.this, Message.INSTANCE.get("settings.error.title"), Message.INSTANCE.get("settings.error.message"));
                } else {
                    Intent intent = new Intent(SettingsActivity.this, HomeActivity.class);

                    int selectedId = goalRadioGroup.getCheckedRadioButtonId();
                    switch (selectedId) {
                        case R.id.a_setting_muscle_up:
                            intent.putExtra(TRAINING_TARGET, TrainingTarget.FORCE_INCREASE);
                            break;
                        case R.id.a_setting_weight_loss:
                            intent.putExtra(TRAINING_TARGET, TrainingTarget.WEIGHT_REDUCTION);
                            break;
                        case R.id.a_setting_fit_and_health:
                            intent.putExtra(TRAINING_TARGET, TrainingTarget.CONDITION_IMPROVE);
                            break;
                    }

                    int intAge = Integer.parseInt(age.getText().toString());
                    intent.putExtra(AGE, intAge);

                    float floatHeight = Float.parseFloat(height.getText().toString()) / 100.0f;
                    intent.putExtra(HEIGHT, floatHeight);

                    float floatWeight = Float.parseFloat(weight.getText().toString());
                    intent.putExtra(WEIGHT, floatWeight);

                    ArrayList<DayOfWeek> selectedDays = new ArrayList<>();
                    for (DayOfWeek dayOfWeek : days.keySet())
                        if (days.get(dayOfWeek).isChecked())
                            selectedDays.add(dayOfWeek);
                    intent.putExtra(DAYS, selectedDays);

                    if (isRestore)
                        intent.putExtra(CORRECT, true);

                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }

    private boolean isGoalValid(RadioGroup goalRadioGroup) {
        return goalRadioGroup.getCheckedRadioButtonId() != -1;
    }

    private boolean isAgeValid(EditText age) {
        return age.getText().toString().length() != 0;
    }

    private boolean isHeightValid(EditText height) {
        return height.getText().toString().length() != 0;
    }

    private boolean isWeightValid(EditText weight) {
        return weight.getText().toString().length() != 0;
    }

    private boolean isAvailableDaysValid(Collection<CheckBox> days) {
        for (CheckBox checkBox : days)
            if (checkBox.isChecked())
                return true;

        return false;
    }
}
