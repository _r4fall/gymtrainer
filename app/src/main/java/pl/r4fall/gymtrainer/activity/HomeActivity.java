package pl.r4fall.gymtrainer.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.GridLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import pl.r4fall.gymtrainer.R;
import pl.r4fall.gymtrainer.element.ActivityAlert;
import pl.r4fall.gymtrainer.element.PositionStyle;
import pl.r4fall.gymtrainer.logic.TrainingPlanCreator;
import pl.r4fall.gymtrainer.logic.UserProfileIO;
import pl.r4fall.gymtrainer.model.Exercise;
import pl.r4fall.gymtrainer.model.Training;
import pl.r4fall.gymtrainer.model.TrainingPlan;
import pl.r4fall.gymtrainer.model.User;
import pl.r4fall.gymtrainer.model.spec.DayOfWeek;
import pl.r4fall.gymtrainer.model.spec.ExerciseType;
import pl.r4fall.gymtrainer.model.spec.MuscleGroup;
import pl.r4fall.gymtrainer.model.spec.TrainingTarget;
import pl.r4fall.gymtrainer.util.Message;

public class HomeActivity extends Activity {
    public final static String RESTORE_SETTINGS = "restore_settings";
    public final static String TRAINING = "training";
    public final static String SELECTED_DATA = "selectedData";

    private final static int SETTINGS_ACTIVITY = 0;
    private final static int EXERCISE_ACTIVITY = 1;

    private final static float CORRECT_BMI = 21.0f;
    private final static float MAX_MBI = 35.0f;

    private Map<View, Calendar> trainingViews = new HashMap<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        final User user = UserProfileIO.getUserProfile(getFilesDir());
        if (user == null)
            return;

        ImageButton settingsButton = (ImageButton) findViewById(R.id.a_home_settings);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent settingsIntent = new Intent(HomeActivity.this, SettingsActivity.class);
                settingsIntent.putExtra(RESTORE_SETTINGS, true);

                settingsIntent.putExtra(SettingsActivity.TRAINING_TARGET, TrainingTarget.FORCE_INCREASE);
                settingsIntent.putExtra(SettingsActivity.AGE, user.getAge());
                settingsIntent.putExtra(SettingsActivity.HEIGHT, user.getHeight());
                settingsIntent.putExtra(SettingsActivity.WEIGHT, user.getWeight());

                ArrayList<DayOfWeek> days = new ArrayList<>(user.getAvailableDays());
                settingsIntent.putExtra(SettingsActivity.DAYS, days);

                startActivityForResult(settingsIntent, SETTINGS_ACTIVITY);
            }
        });

        TrainingPlan plan = user.getTrainingPlan();
        if (plan == null || plan.getAllTrainings() == null || plan.getAllTrainings().isEmpty()) {
            Intent settingsIntent = new Intent(HomeActivity.this, SettingsActivity.class);
            startActivityForResult(settingsIntent, SETTINGS_ACTIVITY);
        } else {
            Calendar today = Calendar.getInstance();
            Map<Calendar, Training> trainings = user.getTrainingPlan().getAllTrainings();
            int trainingsAfterToday = 0;
            for (Calendar calendar : trainings.keySet())
                if (calendar.after(today))
                    trainingsAfterToday++;

            if (trainingsAfterToday < 3) {
                Map<Calendar, Training> newTrainings = TrainingPlanCreator.getTrainings(trainings, user.getAvailableDays(), 3 - trainingsAfterToday,
                        user.getTrainingTarget(), user.getHeight(), user.getHeight(), user.getAge());
                user.getTrainingPlan().getAllTrainings().putAll(newTrainings);
            }
            updateView();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SETTINGS_ACTIVITY && resultCode == RESULT_OK) {
            TrainingTarget trainingTarget = (TrainingTarget) data.getSerializableExtra(SettingsActivity.TRAINING_TARGET);
            int age = data.getIntExtra(SettingsActivity.AGE, 25);
            float height = data.getFloatExtra(SettingsActivity.HEIGHT, 1.8f);
            float weight = data.getFloatExtra(SettingsActivity.WEIGHT, 90.0f);
            ArrayList days = (ArrayList) data.getSerializableExtra(SettingsActivity.DAYS);
            boolean isCorrect = data.getBooleanExtra(SettingsActivity.CORRECT, false);

            User user = UserProfileIO.getUserProfile(getFilesDir());
            if (user == null)
                return;

            user.setTrainingTarget(trainingTarget);
            user.setAge(age);
            user.setHeight(height);
            user.setWeight(weight);

            Set<DayOfWeek> availableDays = new HashSet<>(days.size());
            for (Object day : days)
                availableDays.add((DayOfWeek) day);

            user.setAvailableDays(availableDays);

            if (!isCorrect) {
                Map<Calendar, Training> trainings = TrainingPlanCreator.getTrainings(new HashMap<Calendar, Training>(), availableDays, 3, trainingTarget, weight, height, age);
                user.setTrainingPlan(new TrainingPlan(trainings));
            }

            UserProfileIO.saveUserProfile(getFilesDir(), user);
            updateView();
        } else if (requestCode == EXERCISE_ACTIVITY && resultCode == RESULT_OK) {
            int cardio = data.getIntExtra(ExerciseActivity.RES_TOTAL_CARDIO, 0);
            int ex1 = data.getIntExtra(ExerciseActivity.RES_TOTAL_SERIES_EX_1, 0);
            int ex2 = data.getIntExtra(ExerciseActivity.RES_TOTAL_SERIES_EX_2, 0);
            Calendar calendar = (Calendar) data.getSerializableExtra(ExerciseActivity.RES_DATA);

            User user = UserProfileIO.getUserProfile(getFilesDir());
            if (user == null)
                return;

            Training training = user.getTrainingPlan().getAllTrainings().get(calendar);
            training.setCardioSecs(cardio);
            training.setGroupOneSeries(ex1);
            training.setGroupTwoSeries(ex2);
            UserProfileIO.saveUserProfile(getFilesDir(), user);

            updateView();
        }
    }

    private void updateView() {
        User user = UserProfileIO.getUserProfile(getFilesDir());
        if (user == null)
            return;

        Map<Calendar, Training> trainings = user.getTrainingPlan().getAllTrainings();
        List<Calendar> trainingDays = new ArrayList<>(trainings.keySet());
        Collections.sort(trainingDays);
        Collections.reverse(trainingDays);

        ProgressBar progressBar = (ProgressBar) findViewById(R.id.a_home_progress_bar);
        TextView totalTime = (TextView) findViewById(R.id.a_home_total_time);
        TextView totalSeries = (TextView) findViewById(R.id.a_home_total_series);
        TextView bmi = (TextView) findViewById(R.id.a_home_bmi);

        progressBar.setProgress(getProgress(user.getTrainingTarget(), user.getTrainingPlan(), user.getHeight(), user.getWeight()));
        totalTime.setText(getStringTime(getTotalCardioMin(user.getTrainingPlan())));
        totalSeries.setText(String.valueOf(getTotalDoneSeries(user.getTrainingPlan())));
        bmi.setText(String.valueOf(getBmi(user.getHeight(), user.getWeight())));

        LinearLayout layout = (LinearLayout) findViewById(R.id.a_scroll_layout);
        layout.removeAllViewsInLayout();
        for (Calendar day : trainingDays) {
            View dayRow = getOneDayRowView(day, user.getTrainingPlan());
            layout.addView(dayRow, PositionStyle.LL_MATCH_WRAP_MTOP);

            View line = new View(this);
            line.setBackgroundColor(getResources().getColor(R.color.colorText));
            layout.addView(line, PositionStyle.VG_HLINE_MATCH);
        }
    }

    private View getOneDayRowView(Calendar day, final TrainingPlan trainingPlan) {
        LinearLayout dayRow = new LinearLayout(this);
        dayRow.setOrientation(LinearLayout.HORIZONTAL);

        StringBuilder date = new StringBuilder();
        date.append(day.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.ENGLISH).toUpperCase()).append(" ");
        date.append(day.get(Calendar.DAY_OF_MONTH)).append(" ");
        date.append(day.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH).toUpperCase()).append(" ");
        date.append(day.get(Calendar.YEAR));

        TextView dateTextView = new TextView(this);
        dateTextView.setText(date.toString());
        dateTextView.setTextColor(getResources().getColor(R.color.colorText));
        dayRow.addView(dateTextView, PositionStyle.LL_WRAP_WRAP_GBOTTOM);
        dayRow.addView(getDetailsView(day, trainingPlan), PositionStyle.VG_MATCH_WRAP);

        trainingViews.put(dayRow, day);

        final Calendar today = Calendar.getInstance();
        dayRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(HomeActivity.this, ExerciseActivity.class);
                Calendar selected = trainingViews.get(v);
                if (selected.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) &&
                        selected.get(Calendar.YEAR) == today.get(Calendar.YEAR)) {
                    Training training = trainingPlan.getAllTrainings().get(selected);
                    intent.putExtra(TRAINING, training);
                    intent.putExtra(SELECTED_DATA, selected);
                    startActivityForResult(intent, EXERCISE_ACTIVITY);
                } else {
                    ActivityAlert.showAlert(HomeActivity.this, Message.INSTANCE.get("home.notToday.title"), Message.INSTANCE.get("home.notToday.message"));
                }
            }
        });

        return dayRow;
    }

    private View getDetailsView(Calendar day, TrainingPlan trainingPlan) {
        LinearLayout details = new LinearLayout(this);
        details.setOrientation(LinearLayout.VERTICAL);
        details.setGravity(Gravity.RIGHT);

        GridLayout detailsTable = new GridLayout(this);
        detailsTable.setColumnCount(2);

        Map<Calendar, Training> trainings = trainingPlan.getAllTrainings();
        List<Exercise> exercises = trainings.get(day).getExercises();
        Set<MuscleGroup> muscleGroups = new HashSet<>();
        for (Exercise exercise : exercises) {
            muscleGroups.add(exercise.getExerciseType().getMuscleGroup());
        }
        List<MuscleGroup> muscles = new ArrayList<>(muscleGroups);
        Collections.sort(muscles);
        Collections.reverse(muscles);

        for (MuscleGroup muscleGroup : muscles) {
            TextView groupText = new TextView(this);
            groupText.setText(muscleGroup.name());
            groupText.setTextColor(getResources().getColor(R.color.colorText));
            groupText.setWidth(PositionStyle.getPixelSize(90, getResources().getDisplayMetrics()));
            detailsTable.addView(groupText, PositionStyle.LL_WRAP_WRAP);

            TextView detailText = new TextView(this);
            String seriesStan;
            if (muscleGroup == MuscleGroup.CARDIO)
                seriesStan = getCardioStan(trainings.get(day)) + "min";
            else
                seriesStan = getExerciseTodoSeries(trainings.get(day), muscleGroup) + "ser";
            detailText.setText(seriesStan);
            detailText.setTextColor(getResources().getColor(R.color.colorText));
            detailText.setWidth(PositionStyle.getPixelSize(60, getResources().getDisplayMetrics()));
            detailsTable.addView(detailText, PositionStyle.LL_WRAP_WRAP);
        }

        details.addView(detailsTable, PositionStyle.VG_WRAP_WRAP);

        return details;
    }

    private int getCardioStan(Training training) {
        for (Exercise exercise : training.getExercises())
            if (exercise.getExerciseType() == ExerciseType.CARDIO)
                return exercise.getRepeat();

        return 0;
    }

    private int getExerciseTodoSeries(Training training, MuscleGroup muscleGroup) {
        int series = 0;

        for (Exercise exercise : training.getExercises())
            if (exercise.getExerciseType().getMuscleGroup() == muscleGroup)
                series += exercise.getTodoSeries();

        return series;
    }

    private int getProgress(TrainingTarget target, TrainingPlan trainingPlan, float height, float weight) {
        int cardioMin = getTotalCardioMin(trainingPlan);
        int doneSeries = getTotalDoneSeries(trainingPlan);

        if (target == TrainingTarget.FORCE_INCREASE) {
            return Math.min(100, doneSeries / 10000);
        } else if (target == TrainingTarget.CONDITION_IMPROVE) {
            return Math.min(50, doneSeries / 50000) + Math.min(50, cardioMin / 60000);
        } else {
            float diff = getBmi(height, weight) - CORRECT_BMI;
            if (diff <= 0)
                return 100;
            else if (diff >= MAX_MBI) {
                return 0;
            } else {
                return (int) (100 * (1 - diff / (MAX_MBI - CORRECT_BMI)));
            }

        }
    }

    private int getTotalCardioMin(TrainingPlan trainingPlan) {
        int cardioMin = 0;

        Map<Calendar, Training> trainings = trainingPlan.getAllTrainings();
        for (Training training : trainings.values()) {
            cardioMin += training.getCardioSecs() / 60;
        }

        return cardioMin;
    }

    private int getTotalDoneSeries(TrainingPlan trainingPlan) {
        int doneSeries = 0;

        Map<Calendar, Training> trainings = trainingPlan.getAllTrainings();
        for (Training training : trainings.values()) {
            doneSeries += training.getGroupOneSeries();
            doneSeries += training.getGroupTwoSeries();
        }

        return doneSeries;
    }

    private String getStringTime(int mins) {
        return (mins / 60) + "h" + (mins % 60) + "m";
    }

    private float getBmi(float height, float weight) {
        float bmi = weight / (height * height);
        BigDecimal bd = new BigDecimal(Float.toString(bmi));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);

        return bd.floatValue();
    }
}