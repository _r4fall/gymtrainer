package pl.r4fall.gymtrainer.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;

import pl.r4fall.gymtrainer.R;
import pl.r4fall.gymtrainer.element.ActivityAlert;
import pl.r4fall.gymtrainer.util.Message;

public class NewUserActivity extends Activity {

    public final static String PIN = "pin";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        try {
            Message.INSTANCE.init(getAssets().open("messages.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        final EditText pin = (EditText) findViewById(R.id.a_new_user_pin);
        final EditText confirmPin = (EditText) findViewById(R.id.a_new_user_confirmPin);
        Button okButton = (Button) findViewById(R.id.a_new_user_button_ok);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringPin = pin.getText().toString();
                String stringConfirmPin = confirmPin.getText().toString();

                if (stringPin.length() == 0 || stringConfirmPin.length() == 0) {
                    ActivityAlert.showAlert(NewUserActivity.this, Message.INSTANCE.get("newUser.invalidPin.title"), Message.INSTANCE.get("newUser.invalidPin.message"));
                } else if (!stringPin.equals(stringConfirmPin)) {
                    ActivityAlert.showAlert(NewUserActivity.this, Message.INSTANCE.get("newUser.mismatchedPin.title"), Message.INSTANCE.get("newUser.mismatchedPin.message"));
                } else {
                    Intent intent = new Intent(NewUserActivity.this, MainActivity.class);
                    intent.putExtra(PIN, pin.getText().toString());
                    setResult(RESULT_OK, intent);
                    finish();

                }
            }
        });
    }
}
