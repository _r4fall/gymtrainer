package pl.r4fall.gymtrainer.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.r4fall.gymtrainer.R;
import pl.r4fall.gymtrainer.element.PositionStyle;

public class InfoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);

        Intent intent = getIntent();
        String title = intent.getStringExtra(ExerciseActivity.INFO_TITLE);
        String msg = intent.getStringExtra(ExerciseActivity.INFO_MESSAGE);
        String[] items = msg.split("\\n");

        TextView textView = (TextView) findViewById(R.id.a_info_title);
        textView.setText(title);

        LinearLayout layout = (LinearLayout) findViewById(R.id.a_info_scroll_layout);

        for (int i = 0; i < items.length; i++) {
            LinearLayout numLayout = new LinearLayout(this);
            numLayout.setOrientation(LinearLayout.HORIZONTAL);

            TextView num = new TextView(this);
            num.setText(String.valueOf((i + 1) + ". "));
            num.setTextColor(getResources().getColor(R.color.colorText));
            numLayout.addView(num);

            TextView content = new TextView(this);
            content.setText(items[i]);
            content.setTextColor(getResources().getColor(R.color.colorText));
            numLayout.addView(content);
            layout.addView(numLayout, PositionStyle.LL_MATCH_WRAP_MTOP);
        }

        ImageButton button = (ImageButton) findViewById(R.id.a_info_ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InfoActivity.this.finish();
            }
        });
    }
}
