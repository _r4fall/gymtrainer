package pl.r4fall.gymtrainer.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.mindrot.jbcrypt.BCrypt;

import java.io.IOException;

import pl.r4fall.gymtrainer.R;
import pl.r4fall.gymtrainer.element.ActivityAlert;
import pl.r4fall.gymtrainer.util.Message;

public class PinActivity extends Activity {
    public final static String USER_PASS = "pass";
    public final static String PASS_STATUS = "status";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pin);

        try {
            Message.INSTANCE.init(getAssets().open("messages.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        final EditText pin = (EditText) findViewById(R.id.a_pin_pin);
        Button okButton = (Button) findViewById(R.id.a_pin_button_ok);

        Intent intent = getIntent();
        final String hashedPass = intent.getStringExtra(USER_PASS);

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String stringPin = pin.getText().toString();

                if (stringPin.length() == 0) {
                    ActivityAlert.showAlert(PinActivity.this, Message.INSTANCE.get("pin.emptyPin.title"), Message.INSTANCE.get("pin.emptyPin.message"));
                } else if (!BCrypt.checkpw(stringPin, hashedPass)) {
                    ActivityAlert.showAlert(PinActivity.this, Message.INSTANCE.get("pin.invalidPin.title"), Message.INSTANCE.get("pin.invalidPin.message"));
                } else {
                    Intent intent = new Intent(PinActivity.this, MainActivity.class);
                    intent.putExtra(PASS_STATUS, RESULT_OK);
                    setResult(RESULT_OK, intent);
                    finish();
                }
            }
        });
    }
}
