package pl.r4fall.gymtrainer.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import pl.r4fall.gymtrainer.R;
import pl.r4fall.gymtrainer.element.PositionStyle;
import pl.r4fall.gymtrainer.logic.CountDownTimerWithPause;
import pl.r4fall.gymtrainer.model.Exercise;
import pl.r4fall.gymtrainer.model.Training;
import pl.r4fall.gymtrainer.model.spec.ExerciseType;
import pl.r4fall.gymtrainer.model.spec.MuscleGroup;

public class ExerciseActivity extends Activity {
    public final static String INFO_TITLE = "info_title";
    public final static String INFO_MESSAGE = "info_message";

    public final static String RES_TOTAL_SERIES_EX_1 = "totalSeriesEx1";
    public final static String RES_TOTAL_SERIES_EX_2 = "totalSeriesEx2";
    public final static String RES_TOTAL_CARDIO = "totalCardio";
    public final static String RES_DATA = "resData";

    private final static long MILS_IN_MINUTE = 60 * 1000;

    private Map<View, ExerciseType> exerciseTypeMap;
    private Properties props;
    private int totalSeriesEx1;
    private int totalSeriesEx2;
    private int totalCardio;

    private CountDownTimerWithPause timer = null;
    private TextView cardioTimeText = null;
    private ProgressBar progressBar = null;

    @Override
    protected void onPause() {
        super.onPause();

        if (timer != null && timer.isRunning() && !timer.isPaused())
            timer.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (timer != null && timer.isRunning() && timer.isPaused())
            timer.resume();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        exerciseTypeMap = new HashMap<>(6);
        props = new Properties();
        try {
            props.load(getAssets().open("exercises.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        Intent intent = getIntent();
        Training training = (Training) intent.getSerializableExtra(HomeActivity.TRAINING);
        final Calendar data = (Calendar) intent.getSerializableExtra(HomeActivity.SELECTED_DATA);

        LinearLayout scrollLayout = (LinearLayout) findViewById(R.id.a_exe_scroll_layout);

        int cardio = 0;
        Set<MuscleGroup> muscleGroups = new HashSet<>(2);
        for (Exercise exercise : training.getExercises())
            if (exercise.getExerciseType().getMuscleGroup() != MuscleGroup.CARDIO)
                muscleGroups.add(exercise.getExerciseType().getMuscleGroup());
            else
                cardio = exercise.getRepeat();

        int i = 0;
        for (MuscleGroup muscleGroup : muscleGroups) {
            scrollLayout.addView(getMuscleGroupWithDetails(muscleGroup, training, i++), PositionStyle.LL_MATCH_WRAP_MTOP);

            View line = new View(this);
            line.setBackgroundColor(getResources().getColor(R.color.colorText));
            scrollLayout.addView(line, PositionStyle.VG_HLINE_MATCH);
        }

        ImageButton button = (ImageButton) findViewById(R.id.a_exercise_ok);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent homeIntent = new Intent(ExerciseActivity.this, HomeActivity.class);
                homeIntent.putExtra(RES_TOTAL_SERIES_EX_1, totalSeriesEx1);
                homeIntent.putExtra(RES_TOTAL_SERIES_EX_2, totalSeriesEx2);
                homeIntent.putExtra(RES_TOTAL_CARDIO, totalCardio);
                homeIntent.putExtra(RES_DATA, data);
                setResult(RESULT_OK, homeIntent);
                finish();
            }
        });

        initCardioView(cardio);
    }

    private void initCardioView(int cardioTime) {
        cardioTimeText = (TextView) findViewById(R.id.a_exercise_timer);
        progressBar = (ProgressBar) findViewById(R.id.a_exercise_progress_bar);
        refreshCardioView(cardioTime * MILS_IN_MINUTE, cardioTime * MILS_IN_MINUTE);

        ImageButton play = (ImageButton) findViewById(R.id.a_exercise_play);
        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (timer.isPaused())
                    timer.resume();
                else
                    timer.pause();
            }
        });

        final int cardio = cardioTime;
        timer = new CountDownTimerWithPause(cardio * MILS_IN_MINUTE, 1000, true) {
            public void onTick(long millisUntilFinished) {
                refreshCardioView(cardio * MILS_IN_MINUTE, millisUntilFinished);
                totalCardio = (int) (cardio * 60 - millisUntilFinished / 1000);
                System.out.println(totalCardio);
            }

            public void onFinish() {
                refreshCardioView(cardio * MILS_IN_MINUTE);
            }
        }.create();
        timer.pause();
    }

    private View getMuscleGroupWithDetails(MuscleGroup muscleGroup, Training training, int groupId) {
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                PositionStyle.getPixelSize(25, getResources().getDisplayMetrics()),
                PositionStyle.getPixelSize(25, getResources().getDisplayMetrics())
        );
        layoutParams.gravity = Gravity.CENTER;
        layoutParams.rightMargin = 10;

        LinearLayout groupLayout = new LinearLayout(this);
        groupLayout.setOrientation(LinearLayout.HORIZONTAL);
        groupLayout.setGravity(Gravity.BOTTOM);

        TextView groupName = new TextView(this);
        groupName.setText(muscleGroup.name());
        groupName.setTextColor(getResources().getColor(R.color.colorText));
        groupLayout.addView(groupName, PositionStyle.LL_WRAP_WRAP);

        LinearLayout exercisesLayout = new LinearLayout(this);
        exercisesLayout.setOrientation(LinearLayout.VERTICAL);

        for (Exercise exercise : training.getExercises()) {
            if (exercise.getExerciseType().getMuscleGroup() == muscleGroup) {
                exercisesLayout.addView(getExerciseRow(exercise, layoutParams, groupId), PositionStyle.VG_MATCH_WRAP);
            }
        }

        groupLayout.addView(exercisesLayout, PositionStyle.VG_MATCH_WRAP);

        return groupLayout;
    }

    private View getExerciseRow(final Exercise exercise, ViewGroup.LayoutParams params, final int groupId) {
        LinearLayout oneRow = new LinearLayout(this);
        oneRow.setOrientation(LinearLayout.HORIZONTAL);
        oneRow.setGravity(Gravity.RIGHT);

        TextView exName = new TextView(this);
        exName.setText(props.getProperty(exercise.getExerciseType().name() + ".name"));
        exName.setMaxWidth(PositionStyle.getPixelSize(110, getResources().getDisplayMetrics()));
        exName.setSingleLine();
        exName.setEllipsize(TextUtils.TruncateAt.END);
        exName.setTextColor(getResources().getColor(R.color.colorText));
        oneRow.addView(exName);

        ImageButton imageButton = new ImageButton(this);
        imageButton.setImageResource(R.mipmap.info);
        imageButton.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageButton.setBackgroundColor(getResources().getColor(R.color.colorBackground));
        imageButton.setScaleX(0.6f);
        imageButton.setScaleY(0.6f);

        exerciseTypeMap.put(imageButton, exercise.getExerciseType());

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ExerciseType type = exerciseTypeMap.get(view);
                Intent intent = new Intent(ExerciseActivity.this, InfoActivity.class);
                intent.putExtra(INFO_TITLE, props.getProperty(type.name() + ".name"));
                intent.putExtra(INFO_MESSAGE, props.getProperty(type.name() + ".description"));
                startActivity(intent);
            }
        });

        oneRow.addView(imageButton, params);

        for (int i = 0; i < exercise.getTodoSeries(); i++) {
            CheckBox checkBox = new CheckBox(this);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    if (b) {
                        if (groupId == 0)
                            totalSeriesEx1++;
                        else
                            totalSeriesEx2++;
                    } else {
                        if (groupId == 0)
                            totalSeriesEx1--;
                        else
                            totalSeriesEx2--;
                    }
                }
            });
            oneRow.addView(checkBox);
        }

        return oneRow;
    }

    private void refreshCardioView(long totalMils, long leftMils) {
        long minutes = leftMils / MILS_IN_MINUTE;
        long secs = (leftMils % MILS_IN_MINUTE) / 1000;

        String time = (minutes > 9 ? minutes : ("0" + minutes)) + "m" + (secs > 9 ? secs : ("0" + secs)) + "s";
        float percent = ((float) leftMils /  (float) totalMils) * 100.0f;
        cardioTimeText.setText(time);
        progressBar.setProgress(100 - (int) percent);
    }

    private void refreshCardioView(long totalMils) {
        refreshCardioView(totalMils, 0);
    }
}
