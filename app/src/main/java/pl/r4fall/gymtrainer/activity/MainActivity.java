package pl.r4fall.gymtrainer.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import org.mindrot.jbcrypt.BCrypt;

import pl.r4fall.gymtrainer.R;
import pl.r4fall.gymtrainer.logic.UserProfileIO;
import pl.r4fall.gymtrainer.model.User;

public class MainActivity extends Activity {
    public final static int NEW_USER_ACTIVITY = 0;
    public final static int PIN_ACTIVITY = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!UserProfileIO.isUserProfileExist(getFilesDir())) {
            Intent newUserIntent = new Intent(this, NewUserActivity.class);
            startActivityForResult(newUserIntent, NEW_USER_ACTIVITY);
        } else {
            User user = UserProfileIO.getUserProfile(getFilesDir());
            if (user == null)
                return;

            Intent pinIntent = new Intent(this, PinActivity.class);
            pinIntent.putExtra(PinActivity.USER_PASS, user.getHashedPassword());
            startActivityForResult(pinIntent, PIN_ACTIVITY);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == NEW_USER_ACTIVITY && resultCode == RESULT_OK) {
            String password = data.getStringExtra(NewUserActivity.PIN);
            String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
            User user = new User();
            user.setHashedPassword(hashed);

            UserProfileIO.createUserProfile(getFilesDir(), user);
            Intent pinIntent = new Intent(MainActivity.this, PinActivity.class);
            pinIntent.putExtra(PinActivity.USER_PASS, user.getHashedPassword());
            startActivityForResult(pinIntent, PIN_ACTIVITY);
        } else if (requestCode == PIN_ACTIVITY && resultCode == RESULT_OK) {
            Intent homeActivity = new Intent(MainActivity.this, HomeActivity.class);
            startActivity(homeActivity);
        }
    }
}
