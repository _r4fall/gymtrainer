package pl.r4fall.gymtrainer.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum Message {
    INSTANCE;

    private Properties properties = null;

    public void init(InputStream is) {
        if (properties == null) {
            properties = new Properties();
            try {
                properties.load(is);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String get(String prop) {
        return properties.getProperty(prop);
    }
}
