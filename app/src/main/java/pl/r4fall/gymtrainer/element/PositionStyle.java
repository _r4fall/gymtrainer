package pl.r4fall.gymtrainer.element;

import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.ViewGroup;
import android.widget.LinearLayout;

public class PositionStyle {
    public static ViewGroup.LayoutParams VG_HLINE_MATCH = new ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            3
    );

    public static ViewGroup.LayoutParams VG_MATCH_WRAP = new ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
    );

    public static ViewGroup.LayoutParams VG_WRAP_WRAP = new ViewGroup.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
    );

    public static LinearLayout.LayoutParams LL_WRAP_WRAP = new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
    );

    public static LinearLayout.LayoutParams LL_WRAP_WRAP_GBOTTOM = new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.WRAP_CONTENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
    );

    public static LinearLayout.LayoutParams LL_MATCH_WRAP_MTOP = new LinearLayout.LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
    );

    static {
        LL_WRAP_WRAP_GBOTTOM.gravity = Gravity.BOTTOM;
        LL_MATCH_WRAP_MTOP.topMargin = 30;
    }

    public static int getPixelSize(int dp, DisplayMetrics displayMetrics) {
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }
}
