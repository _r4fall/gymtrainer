package pl.r4fall.gymtrainer.model.spec;

public enum TrainingTarget {
    WEIGHT_REDUCTION,
    FORCE_INCREASE,
    CONDITION_IMPROVE
}
