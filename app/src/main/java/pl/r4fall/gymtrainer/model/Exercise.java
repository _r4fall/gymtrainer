package pl.r4fall.gymtrainer.model;

import pl.r4fall.gymtrainer.model.spec.ExerciseType;

import java.io.Serializable;

public class Exercise implements Serializable {
    private ExerciseType exerciseType;
    private int todoSeries;
    private int repeat;

    public Exercise(ExerciseType exerciseType, int todoSeries) {
        this.exerciseType = exerciseType;
        this.todoSeries = todoSeries;
    }

    public Exercise(ExerciseType exerciseType, int repeat, int todoSeries) {
        this.exerciseType = exerciseType;
        this.repeat = repeat;
        this.todoSeries = todoSeries;
    }

    public ExerciseType getExerciseType() {
        return exerciseType;
    }

    public void setExerciseType(ExerciseType exerciseType) {
        this.exerciseType = exerciseType;
    }

    public int getTodoSeries() {
        return todoSeries;
    }

    public void setTodoSeries(int todoSeries) {
        this.todoSeries = todoSeries;
    }

    public int getRepeat() {
        return repeat;
    }

    public void setRepeat(int repeat) {
        this.repeat = repeat;
    }
}
