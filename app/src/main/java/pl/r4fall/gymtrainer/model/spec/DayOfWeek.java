package pl.r4fall.gymtrainer.model.spec;

import java.util.Calendar;

public enum DayOfWeek {
    MONDAY(Calendar.MONDAY),
    TUESDAY(Calendar.TUESDAY),
    WEDNESDAY(Calendar.WEDNESDAY),
    THURSDAY(Calendar.THURSDAY),
    FRIDAY(Calendar.FRIDAY),
    SATURDAY(Calendar.SATURDAY),
    SUNDAY(Calendar.SUNDAY);

    private int id;

    DayOfWeek(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
