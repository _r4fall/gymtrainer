package pl.r4fall.gymtrainer.model.spec;

public enum MuscleGroup {
    ABDOMINALS,
    CHEST,
    BACK,
    TRAPS,
    TRICEPS,
    BICEPS,
    LATS,
    LEGS,
    CARDIO
}
