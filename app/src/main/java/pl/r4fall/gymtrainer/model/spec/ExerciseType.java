package pl.r4fall.gymtrainer.model.spec;

import java.util.HashSet;
import java.util.Set;

public enum ExerciseType {
    PLANK(MuscleGroup.ABDOMINALS),
    BOTTOMS_UP(MuscleGroup.ABDOMINALS),
    WEIGHTED_SUITCASE_CRUNCH(MuscleGroup.ABDOMINALS),
    STANDING_CABLE_LIFT(MuscleGroup.ABDOMINALS),
    SIT_UP(MuscleGroup.ABDOMINALS),
    BARBELL_AB_ROLLOUT(MuscleGroup.ABDOMINALS),

    PUSHUPS(MuscleGroup.CHEST),
    DUMBBELL_BENCH_PRESS(MuscleGroup.CHEST),
    INCLINE_DUMBBELL_PRESS(MuscleGroup.CHEST),
    LOW_CABLE_CROSSOVER(MuscleGroup.CHEST),
    BARBELL_BENCH_PRESS(MuscleGroup.CHEST),
    DUMBBELL_FLYES(MuscleGroup.CHEST),

    AXLE_DEADLIFT(MuscleGroup.BACK),
    HYPEREXTENSIONS(MuscleGroup.BACK),
    SUPERMAN(MuscleGroup.BACK),
    T_BAR_ROW_WITH_HANDLE(MuscleGroup.BACK),
    PENDLAY_ROWN(MuscleGroup.BACK),
    ONE_ARM_DUMBBELL_ROW(MuscleGroup.BACK),

    SMITH_MACHINE_SHRUG(MuscleGroup.TRAPS),
    LEVERAGE_SHRUG(MuscleGroup.TRAPS),
    KETTLEBELL_SUMO_HIGH_PULL(MuscleGroup.TRAPS),
    UPRIGHT_CABLE_ROW(MuscleGroup.TRAPS),
    SCAPULAR_PULL_UP(MuscleGroup.TRAPS),
    BARBELL_SHRUG_BEHIND_THE_BACK(MuscleGroup.TRAPS),

    DECLINE_EZ_BAR_TRICEPS_EXTENSION(MuscleGroup.TRICEPS),
    PARALLEL_BAR_DIP(MuscleGroup.TRICEPS),
    DUMBBELL_FLOOR_PRESS(MuscleGroup.TRICEPS),
    CLOSE_GRIP_BARBELL_BENCH_PRESS(MuscleGroup.TRICEPS),
    BODY_UP(MuscleGroup.TRICEPS),
    TRICEPS_PUSHDOWN_V_BAR_ATTACHMENT(MuscleGroup.TRICEPS),

    INCLINE_HAMMER_CURLS(MuscleGroup.BICEPS),
    WIDE_GRIP_STANDING_BARBELL_CURL(MuscleGroup.BICEPS),
    SPIDER_CURL(MuscleGroup.BICEPS),
    EZ_BAR_CURL(MuscleGroup.BICEPS),
    CONCENTRATION_CURLS(MuscleGroup.BICEPS),
    OVERHEAD_CABLE_CURL(MuscleGroup.BICEPS),

    WIDE_GRIP_PULL_UP(MuscleGroup.LATS),
    CHIN_UP(MuscleGroup.LATS),
    CLOSE_GRIP_FRONT_LAT_PULLDOWN(MuscleGroup.LATS),
    V_BAR_PULLDOWN(MuscleGroup.LATS),
    SHOTGUN_ROW(MuscleGroup.LATS),
    WIDE_GRIP_REAR_PULL_UP(MuscleGroup.LATS),

    CLEAN_FROM_BLOCKS(MuscleGroup.LEGS),
    SINGLE_LEG_PRESS(MuscleGroup.LEGS),
    BARBELL_FULL_SQUAT(MuscleGroup.LEGS),
    BARBELL_WALKING_LUNGE(MuscleGroup.LEGS),
    SMITH_MACHINE_CALF_RAISE(MuscleGroup.LEGS),
    SEATED_CALF_RAISE(MuscleGroup.LEGS),

    CARDIO(MuscleGroup.CARDIO);

    private MuscleGroup muscleGroup;

    ExerciseType(MuscleGroup muscleGroup) {
        this.muscleGroup = muscleGroup;
    }

    public MuscleGroup getMuscleGroup() {
        return muscleGroup;
    }

    public static Set<ExerciseType> getExercises(MuscleGroup group) {
        Set<ExerciseType> exerciseTypes = new HashSet<>();

        ExerciseType[] types = ExerciseType.values();
        for (ExerciseType type : types)
            if (type.getMuscleGroup() == group)
                exerciseTypes.add(type);

        return exerciseTypes;
    }
}
