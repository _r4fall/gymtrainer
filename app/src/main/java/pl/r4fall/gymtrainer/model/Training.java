package pl.r4fall.gymtrainer.model;

import java.io.Serializable;
import java.util.List;

public class Training implements Serializable {
    private List<Exercise> exercises;
    private int cardioSecs;
    private int groupOneSeries;
    private int groupTwoSeries;

    public List<Exercise> getExercises() {
        return exercises;
    }

    public void setExercises(List<Exercise> exercises) {
        this.exercises = exercises;
    }

    public int getCardioSecs() {
        return cardioSecs;
    }

    public void setCardioSecs(int cardioSecs) {
        this.cardioSecs = cardioSecs;
    }

    public int getGroupOneSeries() {
        return groupOneSeries;
    }

    public void setGroupOneSeries(int groupOneSeries) {
        this.groupOneSeries = groupOneSeries;
    }

    public int getGroupTwoSeries() {
        return groupTwoSeries;
    }

    public void setGroupTwoSeries(int groupTwoSeries) {
        this.groupTwoSeries = groupTwoSeries;
    }
}
