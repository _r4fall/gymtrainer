package pl.r4fall.gymtrainer.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;

public class TrainingPlan implements Serializable {
    private Map<Calendar, Training> allTrainings;

    public TrainingPlan() {
    }

    public TrainingPlan(Map<Calendar, Training> allTrainings) {
        this.allTrainings = allTrainings;
    }

    public Map<Calendar, Training> getAllTrainings() {
        return allTrainings;
    }

    public void setAllTrainings(Map<Calendar, Training> allTrainings) {
        this.allTrainings = allTrainings;
    }
}
