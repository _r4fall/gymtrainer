package pl.r4fall.gymtrainer.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Map;
import java.util.Set;

import pl.r4fall.gymtrainer.model.spec.DayOfWeek;
import pl.r4fall.gymtrainer.model.spec.TrainingTarget;

public class User implements Serializable {
    private String hashedPassword;
    private float weight;
    private float height;
    private int age;
    private TrainingTarget trainingTarget;
    private Set<DayOfWeek> availableDays;
    private TrainingPlan trainingPlan;

    public Training getTodayTraining() {
        if (trainingPlan == null)
            return null;

        Calendar today = Calendar.getInstance();
        Map<Calendar, Training> trainings = trainingPlan.getAllTrainings();
        for (Calendar calendar : trainings.keySet())
            if (calendar.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR) &&
                    calendar.get(Calendar.YEAR) == today.get(Calendar.YEAR))
                return trainings.get(calendar);

        return null;
    }

    public String getHashedPassword() {
        return hashedPassword;
    }

    public void setHashedPassword(String hashedPassword) {
        this.hashedPassword = hashedPassword;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public TrainingTarget getTrainingTarget() {
        return trainingTarget;
    }

    public void setTrainingTarget(TrainingTarget trainingTarget) {
        this.trainingTarget = trainingTarget;
    }

    public Set<DayOfWeek> getAvailableDays() {
        return availableDays;
    }

    public void setAvailableDays(Set<DayOfWeek> availableDays) {
        this.availableDays = availableDays;
    }

    public TrainingPlan getTrainingPlan() {
        return trainingPlan;
    }

    public void setTrainingPlan(TrainingPlan trainingPlan) {
        this.trainingPlan = trainingPlan;
    }
}
