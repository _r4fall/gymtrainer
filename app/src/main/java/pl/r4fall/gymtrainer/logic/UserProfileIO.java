package pl.r4fall.gymtrainer.logic;

import android.support.annotation.Nullable;

import pl.r4fall.gymtrainer.model.User;

import java.io.*;

public class UserProfileIO {
    private final static String PROFILE_FILENAME = ".profile";

    public static boolean isUserProfileExist(File filesDir) {
        File profileFile = new File(filesDir, PROFILE_FILENAME);

        return profileFile.exists();
    }

    public static void createUserProfile(File filesDir, User user) {
        File file = new File(filesDir, PROFILE_FILENAME);
        try {
            if (!file.exists())
                file.createNewFile();

            FileOutputStream fileOutputStream = new FileOutputStream(file);
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(user);
            objectOutputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static void saveUserProfile(File filesDir, User user) {
        removeUserProfile(filesDir);
        createUserProfile(filesDir, user);
    }

    @Nullable
    public static User getUserProfile(File filesDir) {
        File file = new File(filesDir, PROFILE_FILENAME);
        if (!file.exists())
            return null;

        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            User user = (User) objectInputStream.readObject();
            objectInputStream.close();

            return user;
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public static void removeUserProfile(File filesDir) {
        File file = new File(filesDir, PROFILE_FILENAME);
        if (file.exists())
            file.delete();
    }
}
