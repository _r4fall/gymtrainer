package pl.r4fall.gymtrainer.logic;

import android.support.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import pl.r4fall.gymtrainer.model.Exercise;
import pl.r4fall.gymtrainer.model.Training;
import pl.r4fall.gymtrainer.model.spec.DayOfWeek;
import pl.r4fall.gymtrainer.model.spec.ExerciseType;
import pl.r4fall.gymtrainer.model.spec.MuscleGroup;
import pl.r4fall.gymtrainer.model.spec.TrainingTarget;

public class TrainingPlanCreator {
    private final static Set<HashSet<MuscleGroup>> muscleSets = new HashSet<>(Arrays.asList(
            new HashSet<>(Arrays.asList(MuscleGroup.BICEPS, MuscleGroup.TRICEPS)),
            new HashSet<>(Arrays.asList(MuscleGroup.CHEST, MuscleGroup.LATS)),
            new HashSet<>(Arrays.asList(MuscleGroup.LEGS, MuscleGroup.TRAPS)),
            new HashSet<>(Arrays.asList(MuscleGroup.ABDOMINALS, MuscleGroup.BACK))
    ));

    public static Map<Calendar, Training> getTrainings(Map<Calendar, Training> previousTrainings, Set<DayOfWeek> availableDays, int days, TrainingTarget target, float weight, float height, int age) {
        Map<Calendar, Training> nextTrainings = new HashMap<>(days);

        float bmi = weight / (height * height);
        Exercise cardio = getCardioExercise(target, bmi, age);
        int repeats = getRepeats(target, age);

        for (int i = 0; i < days; i++) {
            Training training = new Training();
            List<Exercise> exercises = new ArrayList<>();

            if (cardio != null)
                exercises.add(cardio);

            Set<MuscleGroup> nextMuscleGroup = getNextMuscleGroup(previousTrainings);
            for (MuscleGroup group : nextMuscleGroup) {
                List<ExerciseType> groupExercises = new ArrayList<>(ExerciseType.getExercises(group));
                Collections.shuffle(groupExercises);
                for (int j = 0; j < 3; j++)
                    exercises.add(new Exercise(groupExercises.get(j), repeats, 4));
            }

            training.setExercises(exercises);

            Calendar nextTrainingDate = getNextTrainingDate(previousTrainings.keySet(), availableDays);

            nextTrainings.put(nextTrainingDate, training);
            previousTrainings.put(nextTrainingDate, training);
        }

        return nextTrainings;
    }

    @Nullable
    private static Exercise getCardioExercise(TrainingTarget target, float bmi, int age) {
        if (bmi < 18.5 || target == TrainingTarget.FORCE_INCREASE)
            return new Exercise(ExerciseType.CARDIO, 5, 1);

        Exercise cardio = new Exercise(ExerciseType.CARDIO, 15, 1);
        if (bmi >= 30 && bmi < 35)
            cardio.setRepeat(cardio.getRepeat() + 10);
        else if (bmi >= 35 && bmi < 40)
            cardio.setRepeat(cardio.getRepeat() + 15);
        else if (bmi >= 40)
            cardio.setRepeat(cardio.getRepeat() + 20);

        if (target == TrainingTarget.WEIGHT_REDUCTION)
            cardio.setRepeat(cardio.getRepeat() + 25);
        else if (target == TrainingTarget.CONDITION_IMPROVE)
            cardio.setRepeat(cardio.getRepeat() + 15);

        if (age > 55)
            cardio.setRepeat(cardio.getRepeat() - 15);
        else if (age > 45)
            cardio.setRepeat(cardio.getRepeat() - 10);
        else if (age > 35)
            cardio.setRepeat(cardio.getRepeat() - 5);

        return cardio;
    }

    private static int getRepeats(TrainingTarget target, int age) {
        int repeats = 12;

        if (target == TrainingTarget.FORCE_INCREASE)
            repeats += 8;
        else if (target == TrainingTarget.CONDITION_IMPROVE)
            repeats += 3;

        if (age > 55)
            repeats -= 6;
        else if (age > 45)
            repeats -= 3;

        return repeats;
    }

    private static Set<MuscleGroup> getNextMuscleGroup(Map<Calendar, Training> previousTrainings) {
        Set<HashSet<MuscleGroup>> previousMuscleSet = getPreviousMuscleSet(previousTrainings);
        List<HashSet<MuscleGroup>> availiable = new ArrayList<>(muscleSets);
        availiable.removeAll(previousMuscleSet);
        Collections.shuffle(availiable);

        return availiable.get(0);
    }

    private static Set<HashSet<MuscleGroup>> getPreviousMuscleSet(Map<Calendar, Training> previousTrainings) {
        Set<HashSet<MuscleGroup>> previousMuscleSet = new HashSet<>(muscleSets.size() - 1);

        if (previousTrainings != null) {
            List<Calendar> trainingDays = new ArrayList<>(previousTrainings.keySet());
            Collections.sort(trainingDays);

            for (int i = trainingDays.size(); i > Math.max(trainingDays.size() - muscleSets.size() + 1, 0); i--) {
                Training training = previousTrainings.get(trainingDays.get(i - 1));
                List<Exercise> exercises = training.getExercises();

                HashSet<MuscleGroup> dayMuscleGroup = new HashSet<>(2);
                for (Exercise exercise : exercises)
                    if (exercise.getExerciseType() != ExerciseType.CARDIO)
                        dayMuscleGroup.add(exercise.getExerciseType().getMuscleGroup());

                previousMuscleSet.add(dayMuscleGroup);
            }

            return previousMuscleSet;
        } else {
            return new HashSet<>(0);
        }
    }

    private static Calendar getNextTrainingDate(Set<Calendar> dates, Set<DayOfWeek> availableDays) {
        Calendar today = Calendar.getInstance();
        if (dates == null || dates.isEmpty())
            return getFirstAvailableDay(today, availableDays);

        Calendar last = Calendar.getInstance();
        last.setTimeInMillis(0);

        for (Calendar date : dates)
            if (date.after(last))
                last = date;

        if (today.after(last))
            return getFirstAvailableDay(today, availableDays);

        Calendar next = (Calendar) last.clone();
        next.add(Calendar.DATE, 1);
        return getFirstAvailableDay(next, availableDays);
    }

    private static Calendar getFirstAvailableDay(Calendar day, Set<DayOfWeek> availableDays) {
        while (true) {
            for (DayOfWeek dayOfWeek : availableDays)
                if (day.get(Calendar.DAY_OF_WEEK) == dayOfWeek.getId()) {
                    String days = day.toString();
                    System.out.println(days);
                    return day;
                }

            day.add(Calendar.DATE, 1);
        }
    }

}
